var express = require('express')
var app = express()
var xlsxtojson = require("xlsx-to-json")
var xlstojson = require("xls-to-json")
var fs = require("fs")
var Odoo = require("odoo-xmlrpc")


var listFile = ''

let directory = 'files'
let dirBuf = Buffer.from(directory)
var cont = 1

revisarDir()

//asignado a una variable por si hay k pararlo
var intevalo = setInterval(revisarDir, 21600000)//21600 000 son 4 veces al dia

//rutas
app.get('/', function (req, res) {
  res.send('el dockerfile no es viable')
})

//funciones
function revisarDir() {
  fs.readdir(dirBuf, function (err, files) {
    if (err) {
      console.log(err.message)
    } else {
      files == '' ? listFile = null : listFile = files

    }
    console.log('se ha ejecutado', cont++)
    leerFile(listFile)
  })
}

function leerFile(listFile) {
  var registro = []
  if (listFile) {
    console.log(listFile)

    for (let index = 0; index < listFile.length; index++) {
      url = directory + '/' + listFile[index]
      if (getExtencion(listFile[index]) == "xlsx") {
        xlsxtojson({
          input: url,  // entrada xls 
          //output: "temp1.json", // salida json
          output:null, 
          lowerCaseHeaders: true
        }, function (err, result) {
          if (err) {
            console.log(err)
          } else {
            registro[index] = result
          }
        })
      } else if (getExtencion(listFile[index]) == "xls") {
        xlstojson({
          input: url,  // entrada xls 
          //output: "temp.json", // salida json 
          output: null,
          lowerCaseHeaders: true
        }, function (err, result) {
          if (err) {
            console.log(err)
          } else {
            registro[index] = result
          }
        })
      } else {
        console.log('Archivo incorrecto, sera eliminado')

      }
      eliminarFile(url)
      //renombrarFile(url)
    }
    setTimeout(() => {
      //console.log(typeof registro)
      console.log(registro[0])
      enviarFlectra(registro[0])//para probar
      //tomarCampo(registro[0])
    }, 0);

  } else {
    console.log('listFile vasio')
  }

}

function getExtencion(filename) {
  return filename.split('.').pop();
}

function renombrarFile(url) {
  fs.rename(url, url+'.leido', (err) => {
    if (err) throw err;
    console.log('renombrado');
  });
}

function eliminarFile(url) {
  fs.unlink(url, (err) => {
    if (err) throw err;
    console.log('Eliminado');
  })
}

function enviarFlectra(registro) {
  var flectra = new Odoo({
    url:'localhost',
    port:'7073',
    db:'flectra',
    username:'admin',
    password:'admin'
  })
  flectra.connect(function (err) {
    if(err) return console.log(err)
    
    for (let i = 0; i < registro.length; i++) {
      var check_in, check_out
      if (registro[i].Estado=='Entrada') {
        var tiempo=registro[i].Tiempo
        check_in=tiempo.split(' a')[0]
        var tiempo1=registro[++i].Tiempo
        check_out=tiempo1.split(' a')[0]
        
      }else{
        var tiempo=registro[i].Tiempo
        check_out=tiempo.split(' a')[0]
        var tiempo1=registro[++i].Tiempo
        check_in=tiempo1.split(' a')[0]
      }
      console.log(check_in,'1')
      console.log(check_out,'2')
      console.log(dateManupule(check_out), 'm')
      var day_out= dateManupule(check_in)
      var day_in= dateManupule(check_out)
      console.log(day_in, 'in')
      console.log(day_out, 'out')
      //var myday= dateManupule('09/03/2019 05:50:00')
      
      // var myday=new Date('2019-03-07T06:00:00')
      console.log(myday)

      var inParams = []
      // inParams.push([{'id':1,'check_in':new Date(),'check_out':myday}])
      inParams.push([{'employee_id':registro[i].Número,'check_in':day_in,'check_out':day_out}])
      var params=[]
      params.push(inParams)
      flectra.execute_kw('hr.attendance','create',params,function (err, value) {
        if (err) { return console.log(err) }
        console.log('Result: ',value)
      })
    }

  })
}
function dateManupule(date) {
  var separado =formatDate(date)
      var fechaList = formatSimpleDate(separado[0])
      var hora=separado[1]
      var year=fechaList[2]
      var mon=fechaList[1]
      var day=fechaList[0]
      console.log(year)
      console.log(mon)
      console.log(day)
      console.log(hora)
      return (new Date(year+'-'+mon+'-'+day+'T'+hora))
}

function formatDate(tiempo) {
  var i = tiempo.split(' ');
    return i;
}
function formatSimpleDate(tiempo) {
  var i = tiempo.split('/');
    return i;
}

//para probar
function tomarCampo(registro) {
  // console.log(registro[0].Nombre)
  var employees = ''
  for (let i = 0; i < registro.length; i++) {
    employees += registro[i].Número;
  }
  console.log(employees)
  
}

const puerto = process.env.PORT || 3000
app.listen(puerto, function () {
	console.log(`Servidor corriendo por el puerto ${puerto}`)
})
